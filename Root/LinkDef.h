#include <AIDALoop/LoopAlg.h>
#include <AIDALoop/ParticleLevelLoop.h>
#include <AIDALoop/HistMaker.h>
#include <AIDALoop/HistGenerator.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class std::map<std::string,float>+;
#pragma link C++ class std::map<unsigned int,std::string>+;
#pragma link C++ class std::map<std::string,std::tuple<unsigned int,float,float,float> >+;
#pragma link C++ class std::map<std::string,std::tuple<unsigned int,float,float,float,unsigned int,float,float,float> >+;
#pragma link C++ class std::map<std::string,TH1F*>+;
#pragma link C++ class std::map<std::string,TH2F*>+;
#pragma link C++ class std::initializer_list<std::string>+;

#pragma link C++ class AIDA::LoopAlg+;
#pragma link C++ class AIDA::ParticleLevelLoop+;
#pragma link C++ class AIDA::HistMaker+;
#pragma link C++ class AIDA::HistGenerator+;

#endif
