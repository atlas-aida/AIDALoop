/** @file LoopAlg.cxx
 *  @brief AIDA::LoopAlg class implementation
 *
 *  @author Douglas Davis < douglas.davis@cern.ch >
 */

// TL
#include <AIDALoop/LoopAlg.h>
#include <TopLoop/Core/Utils.h>
#include <TopLoop/Core/FileManager.h>

// C++
#include <iostream>
#include <cstdlib>
#include <memory>

// ROOT
#include <TLorentzVector.h>
#include <TFile.h>
#include <TTree.h>

// RootCore
#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <TopDataPreparation/SampleXsectionSvc.h>
#include <PathResolver/PathResolver.h>

AIDA::LoopAlg::LoopAlg() :
  TL::AlgBase() {
  m_outfileName = "unnamed_aida_out.root";
  m_outtreeName = "unnamed_aida_tree";
  m_overrideOTN = false;
  m_doGeneratorWeights = false;
  m_OF_only = false;
  m_OS_only = false;
}

AIDA::LoopAlg::~LoopAlg() {}

TL::STATUS AIDA::LoopAlg::init() {
  TL::Info(__PRETTY_FUNCTION__,"running init()");
  // ALWAYS MUST CALL THIS FUNCTION in init();
  init_core_vars();

  // initialze our custom variables {
  if ( m_isMC ) {
    el_truthMatched = setupTreeVar<TTRV_vec_char> (m_reader,"el_truthMatched");
    el_true_pdg     = setupTreeVar<TTRV_vec_int>  (m_reader,"el_true_pdg");
    el_true_pt      = setupTreeVar<TTRV_vec_float>(m_reader,"el_true_pt");
    el_true_eta     = setupTreeVar<TTRV_vec_float>(m_reader,"el_true_eta");
    mu_truthMatched = setupTreeVar<TTRV_vec_char> (m_reader,"mu_truthMatched");
    mu_true_pdg     = setupTreeVar<TTRV_vec_int>  (m_reader,"mu_true_pdg");
    mu_true_pt      = setupTreeVar<TTRV_vec_float>(m_reader,"mu_true_pt");
    mu_true_eta     = setupTreeVar<TTRV_vec_float>(m_reader,"mu_true_eta");
  }
  met_sumet = setupTreeVar<TTRV_float>(m_reader,"met_sumet");
  // }

  // setup the sample xsec service. the path to the cross-sections
  // file is hard coded, but not expected to change
  std::string xsecFile = PathResolverFindCalibFile("TopDataPreparation/XSection-MC15-13TeV.data");
  SampleXsectionSvc::svc(xsecFile);
  TL::Info(PFUNC,"TopDataPreparation Xsec file being used:",xsecFile);

  auto sampleMetaSvc = std::make_unique<TL::SampleMetaSvc>();

  // hard coded to 1 fb^-1 (easy to scale later)
  m_dataLumi = 1000.0;
  if ( m_isMC ) {
    m_totalWeights = countSumWeights();
    if ( m_doGeneratorWeights ) {
      m_weightsVecTL = generatorVariedSumWeights();
      m_genWnames    = generatorWeightNames();
    }
  }
  m_dsid         = get_dsid();
  m_initialState = sampleMetaSvc->initialStateString(m_dsid);
  m_generator    = sampleMetaSvc->generatorString(m_dsid);
  m_sampleType   = sampleMetaSvc->sampleTypeString(m_dsid);
  m_eventCounter = 0;
  m_totalEntries = fileManager()->rootChain()->GetEntries();

  m_sampleXsection = SampleXsectionSvc::svc()->sampleXsection();

  return TL::STATUS::Good;
}

TL::STATUS AIDA::LoopAlg::setupOutput() {
  // setting up a ROOT file to output at the end of the job
  m_outputFile = new TFile(m_outfileName.c_str(),"UPDATE");

  auto topNtupleTreeName = fileManager()->treeName();
  std::string baseName   = "AIDA_"+topNtupleTreeName;
  std::string baseFname  = "AIDAfk_"+topNtupleTreeName;
  if ( m_overrideOTN ) {
    baseName  = "AIDA_"+m_outtreeName;
    baseFname = "AIDAfk_"+m_outtreeName;
  }
  if ( m_outputFile->GetListOfKeys()->Contains(baseName.c_str()) ) {
    TL::Fatal(__PRETTY_FUNCTION__,
              "The output file already contains an AIDA tree named",baseName,
              "So we're just going to exit");
  }

  m_AIDAflatTree         = new TTree(baseName.c_str(),baseName.c_str());
  m_AIDAflatTreeMCFakes  = new TTree(baseFname.c_str(),baseFname.c_str());
  m_AIDAmetaTree         = new TTree("AIDA_meta","AIDA_meta");

  m_AIDAmetaTree->Branch("dsid",        &m_dsid);
  m_AIDAmetaTree->Branch("initialState",&m_initialState);
  m_AIDAmetaTree->Branch("generator",   &m_generator);
  m_AIDAmetaTree->Branch("sampleType",  &m_sampleType);

  for ( TTree* aidaTree : { m_AIDAflatTree, m_AIDAflatTreeMCFakes } ) {
    aidaTree->Branch("runNumber",     &m_runNumber);
    aidaTree->Branch("SS",            &m_SS);
    aidaTree->Branch("mumu",          &m_mumu);
    aidaTree->Branch("elel",          &m_elel);
    aidaTree->Branch("elmu",          &m_elmu);
    aidaTree->Branch("met",           &m_met);
    aidaTree->Branch("ht",            &m_ht);
    aidaTree->Branch("njets",         &m_njets);
    aidaTree->Branch("nbjets",        &m_nbjets_AT);
    aidaTree->Branch("llpT",          &m_llpT);
    aidaTree->Branch("lleta",         &m_lleta);
    aidaTree->Branch("llpdg",         &m_llpdg);
    aidaTree->Branch("sllpT",         &m_sllpT);
    aidaTree->Branch("slleta",        &m_slleta);
    aidaTree->Branch("sllpdg",        &m_sllpdg);
    aidaTree->Branch("elpT",          &m_elpT);
    aidaTree->Branch("mupT",          &m_mupT);
    aidaTree->Branch("eleta",         &m_eleta);
    aidaTree->Branch("mueta",         &m_mueta);
    //aidaTree->Branch("dphileps",      &m_dphileps);
    //aidaTree->Branch("dRleps",        &m_dRleps);
    //aidaTree->Branch("dphimetll",     &m_dphimetll);
    //aidaTree->Branch("dphimetsll",    &m_dphimetsll);
    aidaTree->Branch("nomWeightwLum", &m_nomWeightwLum);

    if ( m_isNominal ) {
      auto fillWeightBranchMap = [this](auto& weightMap) {
        for ( auto const& entry : weightMap ) {
          this->m_weightBranchMap.emplace("weightSyswLum_"+entry.first,1.0);
        }
      };
      fillWeightBranchMap(weightSyst_pileup);
      fillWeightBranchMap(weightSyst_jvt);
      fillWeightBranchMap(weightSyst_leptonSF);
      fillWeightBranchMap(weightSyst_bTagSF_extrapolation);
      m_reader->SetEntry(0);
      for ( auto const& entry : weightSyst_bTagSF_eigenvars ) {
        for ( unsigned int i = 0; i < (*(*entry.second)).size(); ++i ) {
          m_weightBranchMap.emplace("weightSyswLum_"+entry.first+"_"+std::to_string(i),1.0);
        }
      }
      m_reader->SetEntry(-1);
      for ( auto& entry : m_weightBranchMap ) {
        aidaTree->Branch(entry.first.c_str(),&entry.second);
        if ( m_verbose ) {
          TL::Info("Syst weight branch:",entry.first);
        }
      }

      if ( m_isMC ) {
        if ( m_doGeneratorWeights ) {
          std::string curGenWname;
          for ( std::size_t i = 0; i < m_weightsVecTL.size(); ++i ) {
            // get the name of the generator weight
            // use that for the branch.
            // it if's "?" use the name "genWeightX" where X in the index
            curGenWname = m_genWnames.at(i);
            if ( curGenWname != "?" ) {
              m_genWeightsBranchMap.emplace("weightSyswLum_"+curGenWname,0.0);
            }
            else {
              m_genWeightsBranchMap.emplace("weightSyswLum_genWeight"+std::to_string(i),0.0);
            }
          } // for each entry in weight vector
          for ( auto& entry : m_genWeightsBranchMap ) {
            aidaTree->Branch(entry.first.c_str(),&entry.second);
            if ( m_verbose ) {
              TL::Info("Generator weight branch:",entry.first);
            }
          }
        } // if doGeneratorWeights
      } // if is MC
    } // if isNominal

    // Don't include these variables in the loop algorithm output by
    // default, a CL argument will include them if desired.
    if ( m_fullOutput ) {
      aidaTree->Branch("eventMass",     &m_eventMass);
      aidaTree->Branch("dileptonMass",  &m_dileptonMass);
      aidaTree->Branch("nbjets_manual", &m_nbjets_manual);
      aidaTree->Branch("ljpT",          &m_ljpT);
      aidaTree->Branch("ljeta",         &m_ljeta);
      aidaTree->Branch("llphi",         &m_llphi);
      aidaTree->Branch("sllphi",        &m_sllphi);
      aidaTree->Branch("ljphi",         &m_ljphi);
      aidaTree->Branch("lje",           &m_lje);
      aidaTree->Branch("sljpT",         &m_sljpT);
      aidaTree->Branch("sljeta",        &m_sljeta);
      aidaTree->Branch("sljphi",        &m_sljphi);
      aidaTree->Branch("slje",          &m_slje);
    } // if full output

  } // for the nonfakes and fakes trees

  return TL::STATUS::Good;
}

// ________________________________________________________________
TL::STATUS AIDA::LoopAlg::execute() {

  TL::ProgressPrint(__PRETTY_FUNCTION__,m_eventCounter,m_totalEntries,10);

  m_eventCounter++;

  auto nleps = (*el_pt)->size() + (*mu_pt)->size();
  if ( nleps != 2 ) {
    return TL::STATUS::Skip;
  }

  fillObjects();
  m_finalState.evaluateSelf();
  if (  m_OS_only && m_finalState.leptonPairs().at(0).SS() ) {
    m_finalState.clear();
    return TL::STATUS::Skip;
  }
  if ( m_OF_only && !(m_finalState.leptonPairs().at(0).elmu()) ) {
    m_finalState.clear();
    return TL::STATUS::Skip;
  }

  fillWeights();
  fillFlatTree();

  if ( m_finalState.hasFakeMuon() || m_finalState.hasFakeElectron() ) {
    m_AIDAflatTreeMCFakes->Fill();
  }
  else {
    m_AIDAflatTree->Fill();
  }

  m_finalState.clear();

  return TL::STATUS::Good;
}

// ________________________________________________________________
TL::STATUS AIDA::LoopAlg::finish() {
  TL::Info(__PRETTY_FUNCTION__,"Total number of events =",m_eventCounter);

  m_AIDAflatTree->Write();
  m_AIDAflatTreeMCFakes->Write();

  if ( !(m_outputFile->GetListOfKeys()->Contains("AIDA_meta")) ) {
    m_AIDAmetaTree->Fill();
    m_AIDAmetaTree->Write();
  }

  m_outputFile->Close();
  return TL::STATUS::Good;
}

// ________________________________________________________________
void AIDA::LoopAlg::fillObjects() {
  for ( size_t i = 0; i < (*el_pt)->size(); ++i ) {
    auto pt  = (*el_pt)->at(i);
    auto eta = (*el_eta)->at(i);
    auto phi = (*el_phi)->at(i);
    TL::EDM::Lepton lep;
    lep.set_pdgId(11);
    lep.p().SetPtEtaPhiM(pt,eta,phi,0.511);
    lep.set_charge((*el_charge)->at(i));
    lep.set_cl_eta((*el_cl_eta)->at(i));
    lep.set_topoetcone20((*el_topoetcone20)->at(i));
    lep.set_ptvarcone20((*el_ptvarcone20)->at(i));
    lep.set_d0sig((*el_d0sig)->at(i));
    lep.set_delta_z0_sintheta((*el_delta_z0_sintheta)->at(i));
    m_finalState.addLepton(lep);
    if ( m_isMC ) {
      // not a fake if it's isolated _or_ from photon conv coming off tau/W/Z.
      if ( (*el_truthMatched)->at(i) == false ) m_finalState.setHasFakeElectron(true);
    }
  }
  for ( size_t i = 0; i < (*mu_pt)->size(); ++i ) {
    auto pt  = (*mu_pt)->at(i);
    auto eta = (*mu_eta)->at(i);
    auto phi = (*mu_phi)->at(i);
    TL::EDM::Lepton lep;
    lep.set_pdgId(13);
    lep.p().SetPtEtaPhiM(pt,eta,phi,105.658);
    lep.set_charge((*mu_charge)->at(i));
    lep.set_topoetcone20((*mu_topoetcone20)->at(i));
    lep.set_ptvarcone30((*mu_ptvarcone30)->at(i));
    lep.set_d0sig((*mu_d0sig)->at(i));
    lep.set_delta_z0_sintheta((*mu_delta_z0_sintheta)->at(i));
    m_finalState.addLepton(lep);
    if ( m_isMC ) {
      // fake if not isolated
      if ( (*mu_truthMatched)->at(i) == false ) m_finalState.setHasFakeMuon(true);
    }
  }
  for ( size_t i = 0; i < (*jet_pt)->size(); ++i ) {
    auto pt  = (*jet_pt)->at(i);
    auto eta = (*jet_eta)->at(i);
    auto phi = (*jet_phi)->at(i);
    auto ene = (*jet_e)->at(i);
    auto jvt = (*jet_jvt)->at(i);
    //jvt cut: require JVT>0.59 if pt<60GeV and |eta|<2.4
    //https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibration
    if ( pt < 60*TL::GeV && std::fabs(eta) < 2.4 && jvt < 0.59 ) continue;

    TL::EDM::Jet jet;
    jet.p().SetPtEtaPhiE(pt,eta,phi,ene);
    jet.set_mv2c00((*jet_mv2c00)->at(i));
    jet.set_mv2c10((*jet_mv2c10)->at(i));
    jet.set_mv2c20((*jet_mv2c20)->at(i));
    jet.set_ip3dsv1((*jet_ip3dsv1)->at(i));
    std::size_t local_jet_isbtagged_77 = (*jet_isbtagged_77)->at(i);
    jet.set_isbtagged_77(local_jet_isbtagged_77);
    m_finalState.addJet(jet);
  }

  m_finalState.MET().p().SetPtEtaPhiM(*(*met_met),0.0,*(*met_phi),0.0);
}

// ________________________________________________________________
void AIDA::LoopAlg::fillWeights() {
  if ( m_isMC ) {
    //todo: find the xsec in init() using dsid from sumWeights tree
    float xsec = m_sampleXsection->getXsection(*(*mcChannelNumber));
    m_lumiWeight = xsec * m_dataLumi / m_totalWeights;
    m_nomWeight  = *(*weight_mc) *
      *(*weight_pileup)    *
      *(*weight_leptonSF)  *
      *(*weight_bTagSF_77) *
      *(*weight_jvt);

    m_nomWeightwLum = m_nomWeight * m_lumiWeight;

    if ( m_isNominal ) {
      std::string tempGenWeightName;
      for ( std::size_t i = 0; i < m_weightsVecTL.size(); ++i ) {
        tempGenWeightName = m_genWnames.at(i);
        if ( m_genWnames.at(i) == "?" ) {
          tempGenWeightName = "weightSyswLum_genWeight"+std::to_string(i);
        }
        else {
          tempGenWeightName = "weightSyswLum_"+tempGenWeightName;
        }
        m_genWeightsBranchMap[tempGenWeightName] =
          m_nomWeight * (xsec * m_dataLumi / m_weightsVecTL.at(i));
      }
    }

    if ( m_isNominal ) {
      // This lambda function takes the map<string,shared_ptr<TTRV_xx>>
      // and different nominal weights to calculate a final weight based
      // on the weight systematics
      auto systWeightFiller = [this](const auto& readerMap, float m1, float m2, float m3, float m4) {
        for ( auto const& systW_val : readerMap ) {
          float weightToFill = m1*m2*m3*m4*(this->m_lumiWeight) * (*(*(systW_val.second)));
          this->m_weightBranchMap["weightSyswLum_"+systW_val.first] = weightToFill;
        }
      };
      systWeightFiller(weightSyst_pileup,*(*weight_mc),*(*weight_leptonSF),*(*weight_bTagSF_77),*(*weight_jvt));
      systWeightFiller(weightSyst_jvt,*(*weight_mc),*(*weight_leptonSF),*(*weight_bTagSF_77),*(*weight_pileup));
      systWeightFiller(weightSyst_leptonSF,*(*weight_mc),*(*weight_pileup),*(*weight_jvt),*(*weight_bTagSF_77));
      systWeightFiller(weightSyst_bTagSF_extrapolation,*(*weight_mc),*(*weight_pileup),*(*weight_jvt),*(*weight_leptonSF));

      // since the eigenvars are stored in a vector we can't use our
      // nifty lambda function above, so we'll do it by hand...
      // 6/4/12 eigenvars for B/C/light jets tag/mistag rates
      std::size_t i; float nominalWeight; std::string pfx;
      for ( auto const& systW : weightSyst_bTagSF_eigenvars ) {
        i = 0;
        for ( auto const& systW_eigenvar : *(*(systW.second)) ) {
          pfx = systW.first+"_"+std::to_string(i);
          nominalWeight = m_lumiWeight *
            (*(*weight_mc))*(*(*weight_pileup))*(*(*weight_leptonSF))*(*(*weight_jvt));
          m_weightBranchMap["weightSyswLum_"+pfx] = nominalWeight*systW_eigenvar;
          ++i;
        }
      }

    } //end if (m_isNominal)
  } //end if (m_isMC)
  else {
    m_lumiWeight    = 1.0;
    m_nomWeightwLum = 1.0;
  }
}

// ________________________________________________________________
void AIDA::LoopAlg::fillFlatTree() {
  m_runNumber = *(*runNumber);

  auto llidx  = m_finalState.leadingLeptonIdx();
  auto sllidx = std::abs((int)llidx - 1);

  auto leadingLep    = m_finalState.leptons().at(llidx);
  auto subleadingLep = m_finalState.leptons().at(sllidx);
  auto leptonPair    = m_finalState.leptonPairs().at(0);

  m_SS   = leptonPair.SS();
  m_elel = leptonPair.elel();
  m_mumu = leptonPair.mumu();
  m_elmu = leptonPair.elmu();

  m_eventMass    = m_finalState.M()*TL::toGeV;
  m_ht           = m_finalState.HT()*TL::toGeV;
  m_dileptonMass = leptonPair.m()*TL::toGeV;
  m_njets        = m_finalState.jets().size();
  // we use the c10 b-tagging benchmark
  // as recommended by top reco.
  m_nbjets_manual = m_finalState.nbjets_c10();
  m_nbjets_AT     = m_finalState.nbjets_AT();
  m_met           = m_finalState.MET().pT()*TL::toGeV;
  m_dRleps        = leptonPair.deltaR();
  m_dphileps      = leptonPair.deltaPhi();

  m_dphimetll  = leadingLep.p().DeltaPhi(m_finalState.MET().p());
  m_dphimetsll = subleadingLep.p().DeltaPhi(m_finalState.MET().p());

  m_llpT  = leadingLep.pT()*TL::toGeV;
  m_lleta = leadingLep.eta();
  m_llphi = leadingLep.phi();
  m_llpdg = leadingLep.pdgId();

  m_sllpT  = subleadingLep.pT()*TL::toGeV;
  m_slleta = subleadingLep.eta();
  m_sllphi = subleadingLep.phi();
  m_sllpdg = subleadingLep.pdgId();

  if ( m_elmu ) {
    if ( std::fabs(m_llpdg) == 11 ) {
      m_elpT  = m_llpT;
      m_mupT  = m_sllpT;
      m_eleta = m_lleta;
      m_mueta = m_slleta;
    }
    else {
      m_elpT  = m_sllpT;
      m_mupT  = m_llpT;
      m_eleta = m_slleta;
      m_mueta = m_lleta;
    }
  }
  else {
    m_elpT  = -999;
    m_mupT  = -999;
    m_eleta = -999;
    m_mueta = -999;
  }

  if ( !m_finalState.jets().empty() ) {
    auto leadingJet = m_finalState.jets().at(0);
    m_ljpT  = leadingJet.pT()*TL::toGeV;
    m_ljeta = leadingJet.eta();
    if ( m_fullOutput ) {
      m_ljphi = leadingJet.phi();
      m_lje   = leadingJet.E()*TL::toGeV;
      if ( m_finalState.jets().size() > 1 ) {
        auto subleadingJet = m_finalState.jets().at(1);
        m_sljpT  = subleadingJet.pT()*TL::toGeV;
        m_sljeta = subleadingJet.eta();
        m_sljphi = subleadingJet.phi();
        m_slje   = subleadingJet.E()*TL::toGeV;
      } // if more than one jet
      else {
        AIDA::set_zero(m_sljpT,m_sljeta,m_sljphi,m_slje);
      }
    } // if full output
    else {
      AIDA::set_zero(m_ljphi,m_lje,m_sljpT,m_sljeta,m_sljphi,m_slje);
    }
  } // if we actually have jets
  else {
    AIDA::set_zero(m_ljpT,m_ljeta,m_ljphi,m_lje,m_sljpT,m_sljeta,m_sljphi,m_slje);
  }

}
