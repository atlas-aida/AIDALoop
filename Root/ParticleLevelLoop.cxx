/** @file ParticleLevelLoop.cxx
 *  @brief AIDA::ParticleLevelLoop class implementation
 *
 *  @author Douglas Davis < douglas.davis@cern.ch >
 */

// TL
#include <AIDALoop/ParticleLevelLoop.h>
#include <TopLoop/Core/Utils.h>
#include <TopLoop/Core/FileManager.h>

// RootCore
#include <TopDataPreparation/SampleXsectionSvc.h>

AIDA::ParticleLevelLoop::ParticleLevelLoop() :
  TL::AlgBase() {}

AIDA::ParticleLevelLoop::~ParticleLevelLoop() {}

TL::STATUS AIDA::ParticleLevelLoop::init() {
  TL::Info(__PRETTY_FUNCTION__,"running init()");
  // ALWAYS MUST CALL THIS FUNCTION in init();
  init_core_vars();

  // setup the sample xsec service
  // the path to the cross-sections file is hard coded, but not expected to
  // change
  try {
    std::string rcpath(std::getenv("ROOTCOREBIN"));
    // Old path before TopDataPreparation-00-08-01
    // rcpath.append("/data/TopDataPreparation/XSection-MC15-13TeV-fromSusyGrp.data");
    // New path as of TopDataPreparation-00-08-01
    rcpath.append("/data/TopDataPreparation/XSection-MC15-13TeV.data");
    SampleXsectionSvc::svc(rcpath);
  } catch (std::logic_error e) {
    TL::Fatal(__PRETTY_FUNCTION__,"ROOTCOREBIN is not defined, please configure rootcore");
  }

  m_eventCounter = 0;
  m_totalEntries = fileManager()->rootParticleLevelChain()->GetEntries();

  m_sampleXsection = SampleXsectionSvc::svc()->sampleXsection();
  return TL::STATUS::Good;
}

TL::STATUS AIDA::ParticleLevelLoop::setupOutput() {
  TL::Warning(__PRETTY_FUNCTION__,"ParticleLevelLoop currently does not have output!");
  return TL::STATUS::Good;
}

TL::STATUS AIDA::ParticleLevelLoop::execute() {
  TL::ProgressPrint(__PRETTY_FUNCTION__,m_eventCounter,m_totalEntries,10);

  m_eventCounter++;

  fillObjects();
  m_finalState.evaluateSelf();
  m_finalState.clear();

  return TL::STATUS::Good;
}

TL::STATUS AIDA::ParticleLevelLoop::finish() {
  return TL::STATUS::Good;
}

void AIDA::ParticleLevelLoop::fillObjects() {
  for ( size_t i = 0; i < (*plt_el_pt)->size(); ++i ) {
    auto pt  = (*plt_el_pt)->at(i);
    auto eta = (*plt_el_eta)->at(i);
    auto phi = (*plt_el_phi)->at(i);
    TL::EDM::Lepton lep;
    lep.set_isParticleLevel(true);
    if ( pt > 25*TL::GeV && std::fabs(eta) < 2.5 ) {
      lep.set_partLevPassedSelection(true);
    }
    lep.set_pdgId(11);
    lep.p().SetPtEtaPhiM(pt,eta,phi,0.511);
    lep.set_charge((*plt_el_charge)->at(i));
    m_finalState.addLepton(lep);
  }
  for ( size_t i = 0; i < (*plt_mu_pt)->size(); ++i ) {
    auto pt  = (*plt_mu_pt)->at(i);
    auto eta = (*plt_mu_eta)->at(i);
    auto phi = (*plt_mu_phi)->at(i);
    TL::EDM::Lepton lep;
    lep.set_isParticleLevel(true);
    if ( pt > 25*TL::GeV && std::fabs(eta) < 2.5 ) {
      lep.set_partLevPassedSelection(true);
    }
    lep.set_pdgId(13);
    lep.p().SetPtEtaPhiM(pt,eta,phi,105.658);
    lep.set_charge((*plt_mu_charge)->at(i));
    m_finalState.addLepton(lep);
  }
  for ( size_t i = 0; i < (*plt_jet_pt)->size(); ++i ) {
    auto pt  = (*plt_jet_pt)->at(i);
    auto eta = (*plt_jet_eta)->at(i);
    auto phi = (*plt_jet_phi)->at(i);
    auto en  = (*plt_jet_e)->at(i);
    TL::EDM::Jet jet;
    jet.set_isParticleLevel(true);
    if ( pt > 25*TL::GeV && std::fabs(eta) < 2.5 ) {
      jet.set_partLevPassedSelection(true);
    }
    jet.p().SetPtEtaPhiE(pt,eta,phi,en);
    m_finalState.addJet(jet);
  }
  m_finalState.MET().p().SetPtEtaPhiM(*(*plt_met_met),0.0,*(*plt_met_phi),0.0);
}
