/** @file HistMaker.cxx
 *  @brief AIDA::HistMaker class implementation
 *
 *  @author Douglas Davis < douglas.davis@cern.ch >
 */

// AIDA-Loop
#include <AIDALoop/HistMaker.h>
#include <TopLoop/Core/Utils.h>
#include <TopLoop/json/json.hpp>

// ROOT
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>

// C++
#include <cmath>
#include <string>
#include <memory>
#include <string>
#include <fstream>

AIDA::HistMaker::HistMaker() {
  m_treeName  = "nominal";
  m_lumiScale = 1.0;
  m_channels  = { "ee2015", "uu2015", "eu2015", "ee2016", "uu2016", "eu2016" ,"ee", "uu", "eu" };
  try {
    std::string rcpath(std::getenv("ROOTCOREBIN"));
    rcpath.append("/data/AIDALoop/json/histogramming.json");
    std::ifstream injson(rcpath.c_str());
    auto j = nlohmann::json::parse(injson);
    for ( auto const& proc : j["processes"] ) {
      m_processes.emplace_back(proc.get<std::string>());
    }
    for ( auto const& hist : j.at("histograms") ) {
      std::string  name  = hist.at("name").get<std::string>();
      unsigned int nbins = hist.at("binning").at(0);
      float        xmin  = hist.at("binning").at(1);
      float        xmax  = hist.at("binning").at(2);
      float        epsil = hist.at("binning").at(3);
      m_tabs_1d.emplace(std::make_pair(name,std::make_tuple(nbins,xmin,xmax,epsil)));
    }
  }
  catch (std::logic_error e) {
    TL::Fatal(PFUNC,"ROOTCOREBIN is not defined, please configure rootcore");
  }
  m_tabs_2d = {
    { "met_njets" , std::make_tuple(20,20,220,0.001,8,-0.5,9.5,0.001) }
  };
  TH1::SetDefaultSumw2();
  for ( auto const& ch : m_channels ) {
    for ( auto const& ptype : m_processes ) {
      for ( auto const& tab : m_tabs_1d ) {
        auto htitle_cpp_str = ch+"_"+ptype+"_"+tab.first;
        auto htitle_c_str   = htitle_cpp_str.c_str();
        unsigned int nbins; float xmin; float xmax; float epsilon;
        std::tie(nbins,xmin,xmax,epsilon) = tab.second;
        m_histograms_1d.emplace(htitle_cpp_str,
                                new TH1F(htitle_c_str,htitle_c_str,nbins,xmin,xmax));
      } // for each title and binning
      for ( auto const& tab : m_tabs_2d ) {
        auto htitle_cpp_str = ch+"_"+ptype+"_"+tab.first;
        auto htitle_c_str   = htitle_cpp_str.c_str();
        unsigned int nbins1; float xmin1; float xmax1; float epsilon1;
        unsigned int nbins2; float xmin2; float xmax2; float epsilon2;
        std::tie(nbins1,xmin1,xmax1,epsilon1,nbins2,xmin2,xmax2,epsilon2) = tab.second;
        m_histograms_2d.emplace(htitle_cpp_str,new TH2F(htitle_c_str,htitle_c_str,
                                                        nbins1,xmin1,xmax1,nbins2,xmin2,xmax2));
      } // for each 2d title and binning
    } // for each process
  }
  m_sampleMetaSvc = std::make_shared<TL::SampleMetaSvc>();
}

AIDA::HistMaker::~HistMaker() {}

// _____________________________________________________________________
void AIDA::HistMaker::addFiles(const std::initializer_list<std::string>& fs) {
  std::vector<std::string> fsv(fs);
  addFiles(fsv);
}

// _____________________________________________________________________
void AIDA::HistMaker::addFiles(const std::vector<std::string>& fs) {
  m_treeName = "AIDA_"+m_treeName;
  const char* treeName = m_treeName.c_str();
  TL::Info(PFUNC,"Filling histograms for tree:",treeName);
  for ( auto const& fn : fs ) {
    m_files.emplace_back(TFile::Open(fn.c_str()));
  }
  for ( auto const& f : m_files ) {
    m_metaTrees.emplace_back(dynamic_cast<TTree*>(f->Get("AIDA_meta")));
    m_flatTrees.emplace_back(dynamic_cast<TTree*>(f->Get(treeName)));
  }
}

// _____________________________________________________________________
void AIDA::HistMaker::generate() {
  using vecfloat = std::vector<float>;
  
  TTreeReader metaReader;
  TTreeReaderValue<unsigned int> dsid(metaReader,"dsid");
  
  TTreeReader flatReader;
  TTreeReaderValue<bool>         SS         (flatReader,"SS");
  TTreeReaderValue<unsigned int> runNumber  (flatReader,"runNumber");
  TTreeReaderValue<bool>         elmu       (flatReader,"elmu");
  TTreeReaderValue<bool>         elel       (flatReader,"elel");
  TTreeReaderValue<bool>         mumu       (flatReader,"mumu");
  TTreeReaderValue<float>        met        (flatReader,"met");
  TTreeReaderValue<float>        ht         (flatReader,"ht");
  TTreeReaderValue<unsigned int> njets      (flatReader,"njets");
  TTreeReaderValue<unsigned int> nbjets     (flatReader,"nbjets");
  TTreeReaderValue<float>        dR_leps    (flatReader,"deltaR_leptons");
  TTreeReaderValue<float>        mass       (flatReader,"eventMass");
  
  TTreeReaderValue<float>        llpT       (flatReader,"llpT");
  TTreeReaderValue<float>        lleta      (flatReader,"lleta");
  TTreeReaderValue<float>        llphi      (flatReader,"llphi");

  TTreeReaderValue<float>        sllpT      (flatReader,"sllpT");
  TTreeReaderValue<float>        slleta     (flatReader,"slleta");
  TTreeReaderValue<float>        sllphi     (flatReader,"sllphi");

  TTreeReaderValue<float>        lumiWeight (flatReader,"lumiWeight");
  TTreeReaderValue<vecfloat>     eventWeight(flatReader,"eventWeight");

  for ( size_t i = 0; i < m_flatTrees.size(); ++i ) {
    metaReader.SetTree(m_metaTrees.at(i));
    metaReader.SetEntry(0);

    if ( m_sampleMetaSvc->table().find(*dsid) == m_sampleMetaSvc->table().end() ) {
      TL::Fatal(PFUNC,"You asked for DSID that's not in our table:",*dsid);
    }
    std::string i_process = m_sampleMetaSvc->initialStateString(*dsid);

    flatReader.SetTree(m_flatTrees.at(i));
    flatReader.SetEntry(-1);

    while ( flatReader.Next() ) {
      // cut on channel, for fun..
      // handle varying cuts later, or maybe
      // just rely on AIDA-Loop.
      if ( !(*SS) ) {

        std::string ch_pfx;
        if ( *elmu ) ch_pfx = "eu";
        if ( *elel ) ch_pfx = "ee";
        if ( *mumu ) ch_pfx = "uu";

        //determine run period
        std::string runPd;
        if ( *runNumber >= 297730 ) runPd = "2016";
        if ( *runNumber >= 276262 && *runNumber <= 284484 ) runPd = "2015";

        auto i_llpT   = *llpT,  i_lleta  = *lleta,  i_llphi   = *llphi;
        auto i_sllpT  = *sllpT, i_slleta = *slleta, i_sllphi  = *sllphi;
        auto i_met    = *met,   i_ht     = *ht,     i_dR_leps = *dR_leps, i_mass = *mass;
        auto i_njets  = *njets, i_nbjets = *nbjets;

        // nominal weight and lumi weight to (LoopAlg output is 1 /fb).
        auto i_weight = (*lumiWeight) * (*eventWeight).at(0);
        if ( i_process != "Data" ) { i_weight *= m_lumiScale; }

        auto llpT_fill                = filler(i_llpT,   m_tabs_1d.at("llpT"));
        auto sllpT_fill               = filler(i_sllpT,  m_tabs_1d.at("sllpT"));
        auto met_fill                 = filler(i_met,    m_tabs_1d.at("met"));
        auto ht_fill                  = filler(i_ht,     m_tabs_1d.at("ht"));
        auto dR_leps_fill             = filler(i_dR_leps,m_tabs_1d.at("dR_leps"));
        auto njets_fill               = filler(i_njets,  m_tabs_1d.at("njets"));
        auto nbjets_fill              = filler(i_nbjets, m_tabs_1d.at("nbjets"));
        auto mass_fill                = filler(i_mass,   m_tabs_1d.at("mass"));
        auto njets_2bins_fill         = filler(i_njets,  m_tabs_1d.at("njets_2bins"));
        auto njets_2bins_l30met_fill  = filler(i_njets,  m_tabs_1d.at("njets_2bins_l30met"));
        auto njets_2bins_ge30met_fill = filler(i_njets,  m_tabs_1d.at("njets_2bins_ge30met"));
        auto njets_3bins_fill         = filler(i_njets,  m_tabs_1d.at("njets_3bins"));
        auto njets_3bins_l30met_fill  = filler(i_njets,  m_tabs_1d.at("njets_3bins_l30met"));
        auto njets_3bins_ge30met_fill = filler(i_njets,  m_tabs_1d.at("njets_3bins_ge30met"));
        
        auto i_periodNames = { runPd, std::string("") };

        for ( auto const& pdName : i_periodNames ) {
          auto i_histname = ch_pfx + pdName +  "_" + i_process;
          m_histograms_1d.at(i_histname+"_llpT"   )->Fill(llpT_fill,i_weight);
          m_histograms_1d.at(i_histname+"_lleta"  )->Fill(i_lleta,i_weight);
          m_histograms_1d.at(i_histname+"_llphi"  )->Fill(i_llphi, i_weight);
          m_histograms_1d.at(i_histname+"_sllpT"  )->Fill(sllpT_fill,i_weight);
          m_histograms_1d.at(i_histname+"_slleta" )->Fill(i_slleta,i_weight);
          m_histograms_1d.at(i_histname+"_sllphi" )->Fill(i_sllphi,i_weight);
          m_histograms_1d.at(i_histname+"_njets"  )->Fill(njets_fill,i_weight);
          m_histograms_1d.at(i_histname+"_nbjets" )->Fill(nbjets_fill,i_weight);
          m_histograms_1d.at(i_histname+"_ht"     )->Fill(ht_fill,i_weight);
          m_histograms_1d.at(i_histname+"_met"    )->Fill(met_fill,i_weight);
          m_histograms_1d.at(i_histname+"_dR_leps")->Fill(dR_leps_fill,i_weight);
          m_histograms_1d.at(i_histname+"_mass"   )->Fill(mass_fill,i_weight);

          // different jet and missing ET regions {{
          switch ( i_njets ) {
          case 0:
            m_histograms_1d.at(i_histname+"_met_0j")->Fill(met_fill,i_weight);
            break;
          case 1:
            m_histograms_1d.at(i_histname+"_met_1j"  )->Fill(met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_met_ge1j")->Fill(met_fill,i_weight);
            break;
          case 2:
            m_histograms_1d.at(i_histname+"_met_2j"  )->Fill(met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_met_ge1j")->Fill(met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_met_ge2j")->Fill(met_fill,i_weight);
            break;
          default:
            m_histograms_1d.at(i_histname+"_met_ge1j")->Fill(met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_met_ge2j")->Fill(met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_met_ge3j")->Fill(met_fill,i_weight);
            break;
          }

          if ( i_met < 30 ) {
            m_histograms_1d.at(i_histname+"_njets_l30met"      )->Fill(njets_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_2bins"       )->Fill(njets_2bins_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_2bins_l30met")->Fill(njets_2bins_l30met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_3bins"       )->Fill(njets_3bins_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_3bins_l30met")->Fill(njets_3bins_l30met_fill,i_weight);
          }
          else {
            m_histograms_1d.at(i_histname+"_njets_ge30met"      )->Fill(njets_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_2bins"        )->Fill(njets_2bins_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_2bins_ge30met")->Fill(njets_2bins_ge30met_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_3bins"        )->Fill(njets_3bins_fill,i_weight);
            m_histograms_1d.at(i_histname+"_njets_3bins_ge30met")->Fill(njets_3bins_ge30met_fill,i_weight);
          } // }}

          m_histograms_2d.at(i_histname+"_met_njets")->Fill(met_fill,njets_fill,i_weight);
        }
      }
      else {
        continue;
      }
    } // loop over all entries
  } // loop over all trees
}

// _____________________________________________________________________
void AIDA::HistMaker::makeRatios() {
  std::vector<std::string> procs = { "ttbar", "WW", "Zjets", "Wt", "Wjets", "WZ", "ZZ" };
  int nbins; float xmin; float xmax; float eps;
  for ( auto const& ch : m_channels ) {
    for ( auto const& tab : m_tabs_1d ) {
      auto hist_var = tab.first;
      std::tie(nbins,xmin,xmax,eps) = tab.second;
      auto temp_mc_hist = std::make_shared<TH1F>("temp_mc_hist","temp_mc_hist",nbins,xmin,xmax);
      for ( auto const& proc : procs) {
        temp_mc_hist->Add(m_histograms_1d.at(ch+"_"+proc+"_"+hist_var));
      }
      auto current_ratio = new TH1F((ch+"_ratio_"+hist_var).c_str(),"",nbins,xmin,xmax);
      current_ratio->Add(m_histograms_1d.at(ch+"_Data_"+hist_var));
      current_ratio->Divide(temp_mc_hist.get());
      m_histograms_1d.emplace(std::make_pair(ch+"_ratio_"+hist_var,current_ratio));
    }
  }
}

// _____________________________________________________________________
void AIDA::HistMaker::writeOut(const char* filename) {
  const char* treeName = m_treeName.c_str();
  auto outFile = new TFile(filename,"UPDATE");
  if ( outFile->GetListOfKeys()->Contains(treeName) ) {
    TL::Fatal(PFUNC,treeName,"already exists in the output file, exiting.");
  }
  auto outDir  = outFile->mkdir(treeName);
  outDir->mkdir("ee");
  outDir->mkdir("uu");
  outDir->mkdir("eu");
  outDir->mkdir("ee2015");
  outDir->mkdir("uu2015");
  outDir->mkdir("eu2015");
  outDir->mkdir("ee2016");
  outDir->mkdir("uu2016");
  outDir->mkdir("eu2016");

  generate();
  makeRatios();

  loopToFill(outDir,m_histograms_1d);
  loopToFill(outDir,m_histograms_2d);
  outFile->Close();
}
