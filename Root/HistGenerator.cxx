/** @file HistGenerator.cxx
 *  @brief AIDA::HistGenerator class implementation
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

// ROOT
#include "TTree.h"
#include "TChain.h"
#include "TTreeReader.h"
#include "TROOT.h"

// AIDALoop
#include "AIDALoop/HistGenerator.h"

// C++
#include <iostream>

// boost
#include <boost/algorithm/string.hpp>

AIDA::HistGenerator::HistGenerator() { /* unused default constructor */ }

AIDA::HistGenerator::HistGenerator(const char* outfilename, std::string treename,
                                   const char* textfilename, const float lumscale,
                                   const bool doWeightSysts, bool doSS) {
  m_outFile   = new TFile(outfilename,"UPDATE");
  m_isNominal = false;
  m_doSS = doSS;
  m_doWeightSysts = doWeightSysts;
  m_treename   = "AIDA_" + treename;
  m_treename_f = "AIDAfk_" + treename ;
  if ( m_treename == "AIDA_nominal" ) {
    m_isNominal = true;
  }
  if ( m_outFile->GetListOfKeys()->Contains(m_treename.c_str()) ) {
    TL::Fatal(PFUNC,m_treename,"already exists in the output file, exiting.");
  }
  m_outDir = m_outFile->mkdir(m_treename.c_str());
  std::ifstream infile(textfilename);
  std::string infilename;
  TFile *tfile;
  while ( infile >> infilename ) {
    TL::Info(__FUNCTION__,"Adding file",infilename,"and parsing tree",m_treename);
    tfile = TFile::Open(infilename.c_str(),"READ");
    m_trees.emplace_back(std::make_tuple(dynamic_cast<TTree*>(tfile->Get(m_treename.c_str())),
                                         dynamic_cast<TTree*>(tfile->Get(m_treename_f.c_str())),
                                         dynamic_cast<TTree*>(tfile->Get("AIDA_meta"))));
  }
  m_reader        = std::make_shared<TTreeReader>(std::get<0>(m_trees.at(0)));
  m_metaReader    = std::make_shared<TTreeReader>(std::get<1>(m_trees.at(0)));

  m_dsid          = std::make_shared<TTreeReaderValue<int>>  (*m_metaReader,"dsid");
  m_nomWeightwLum = std::make_shared<TTreeReaderValue<float>>(*m_reader,"nomWeightwLum");
  m_mumu          = std::make_shared<TTreeReaderValue<bool>> (*m_reader,"mumu");
  m_elel          = std::make_shared<TTreeReaderValue<bool>> (*m_reader,"elel");
  m_elmu          = std::make_shared<TTreeReaderValue<bool>> (*m_reader,"elmu");
  m_runNumber     = std::make_shared<TTreeReaderValue<int>>  (*m_reader,"runNumber");
  m_SS            = std::make_shared<TTreeReaderValue<bool>> (*m_reader,"SS");

  m_sampleMetaSvc = std::make_shared<TL::SampleMetaSvc>();
  m_lumscale      = lumscale;

  if ( m_isNominal && m_doWeightSysts ) {
    auto a_tree = std::get<0>(m_trees.at(0));
    auto objArr = a_tree->GetListOfBranches();
    for ( int i = 0; i < objArr->GetEntries(); ++i ) {
      std::string name(objArr->At(i)->GetName());
      if ( boost::algorithm::contains(name,"weightSyswLum_") ) {
        m_weightSysReaders.emplace(name,std::make_shared<TTreeReaderValue<float>>(*m_reader,name.c_str()));
      } // if weightSyswLum_ in the name
    } // for each branch in nom tree
    if ( m_weightSysReaders.empty() ) {
      TL::Fatal(PFUNC,"If you're running weight systematics, the first",
		"file in your -t input cannot be data!");
    }
  } // if is nominal && doWeightSysts
}

AIDA::HistGenerator::~HistGenerator() {}

void AIDA::HistGenerator::parseJson(const char* jsonfilename) {
  std::ifstream infile(jsonfilename);
  TL::Info(PFUNC,"Histogram json config being used:",jsonfilename);
  m_topJson = nlohmann::json::parse(infile);
  for ( const std::string v : m_topJson.at("variables") ) {
    m_variables.emplace(v,std::make_shared<TTreeReaderValue<float>>(*m_reader,v.c_str()));
  }
  for ( const std::string& channel : m_topJson.at("channels") ) {
    for ( const std::string& process : m_topJson.at("processes") ) {
      for ( auto const& j : m_topJson.at("histograms") ) {
        std::string main_hist_name = channel+"_"+process+"_"+(j.at("name").get<std::string>());
        auto hist = std::make_shared<AIDA::Histogram>(main_hist_name.c_str(),
                                                      j.at("bins").at(0).get<int>(),
                                                      j.at("bins").at(1).get<float>(),
                                                      j.at("bins").at(2).get<float>());
        hist->setVar(j.at("var").get<std::string>());
        hist->hasCuts(false);
        if ( !j.at(">cuts").empty() ) {
          for ( auto const& j2 : j.at(">cuts") ) {
            hist->addGreaterThanCut(j2.at("var").get<std::string>(),j2.at("val").get<float>());
            hist->hasCuts(true);
          }
        } // if then for each > cut
        if ( !j.at(">=cuts").empty() ) {
          for ( auto const& j2 : j.at(">=cuts") ) {
            hist->addGreaterThanEqCut(j2.at("var").get<std::string>(),j2.at("val").get<float>());
            hist->hasCuts(true);
          }
        } // if then for each >= cut
        if ( !j.at("<cuts").empty() ) {
          for ( auto const& j2 : j.at("<cuts") ) {
            hist->addLessThanCut(j2.at("var").get<std::string>(),j2.at("val").get<float>());
            hist->hasCuts(true);
          }
        } // if then for each < cut
        if ( !j.at("<=cuts").empty() ) {
          for ( auto const& j2 : j.at("<=cuts") ) {
            hist->addLessThanEqCut(j2.at("var").get<std::string>(),j2.at("val").get<float>());
            hist->hasCuts(true);
          }
        } // if then for each <= cut
        m_aidaHistograms.emplace(main_hist_name,hist);


        // setup histogram for all of the weight systematics in the
        // nominal tree (only for nominal).
        if ( m_isNominal && m_doWeightSysts ) {
          for ( auto const& entry : m_weightSysReaders ) {
            if ( process == "Data" ) continue;
            std::string weightName = entry.first;
            main_hist_name = channel+"_"+process+"_"+(j.at("name").get<std::string>())+"_"+weightName;
            auto weightHist = std::make_shared<AIDA::Histogram>(main_hist_name.c_str(),
                                                                j.at("bins").at(0).get<int>(),
                                                                j.at("bins").at(1).get<float>(),
                                                                j.at("bins").at(2).get<float>());
            m_aidaHistograms.emplace(main_hist_name,weightHist);
          }
        } // if nominal tree and do syst weights

      } // for each histogram
    }// for each process
  } // for each channel
}

TL::STATUS AIDA::HistGenerator::fill() {
  TTree* main_tree = nullptr;
  TTree* fake_tree = nullptr;
  std::string itreename, channel, process, generator, runPd, main_hist_name;
  float epsilon, cval, filler, nom_w;
  std::shared_ptr<AIDA::Histogram> histogram;

  for ( auto const& treePair : m_trees ) {
    m_metaReader->SetTree(std::get<2>(treePair));
    m_metaReader->SetEntry(0);
    process   = m_sampleMetaSvc->initialStateString(*(*m_dsid));
    generator = m_sampleMetaSvc->generatorString(*(*m_dsid));
    if ( process == "WZ" || process == "ZZ" ) process = "Diboson";

    main_tree = std::get<0>(treePair);
    if ( process != "Data" ) {
      fake_tree = std::get<1>(treePair);
    }
    else {
      fake_tree = nullptr;
    }
    std::vector<TTree*> treestoloop;
    if ( process != "Data" ) {
      treestoloop = { main_tree, fake_tree };
    }
    else {
      treestoloop = { main_tree };
    }
    for ( auto const& itree : treestoloop ) {
      if ( process == "Data" && m_treename != "AIDA_nominal" ) continue;
      itreename = itree->GetName();
      if ( boost::algorithm::contains(itreename,"AIDAfk_") ) {
        process = "Fake";
      }
      m_reader->SetTree(itree);
      m_reader->SetEntry(-1);
      TL::Info(PFUNC,"DSID:",*(*m_dsid),"PROCESS:",process,"FROM:",generator);
      while ( m_reader->Next() ) {
        // check sign
        if (  m_doSS && !(*(*m_SS)) ) { continue; }
        if ( !m_doSS &&  (*(*m_SS)) ) { continue; }
        // check channel
        if      ( *(*m_elel) ) channel = "elel";
        else if ( *(*m_mumu) ) channel = "mumu";
        else if ( *(*m_elmu) ) channel = "elmu";
        if      ( *(*m_runNumber) >= 297730 ) runPd  = "2016";
        else if ( *(*m_runNumber) >= 276262 && *(*m_runNumber) <= 284484 ) runPd = "2015";
        auto i_pdNames = { runPd, std::string("") };
        for ( auto const& pdName : i_pdNames ) {

          /////////////////////////////////////////////////////////////////////////////////////
          // This loop takes care of filling every histogram defined in  the input json file //
          for ( auto const& j : m_topJson.at("histograms") ) {
            main_hist_name = channel+pdName+"_"+process+"_"+(j.at("name").get<std::string>());
            histogram = m_aidaHistograms.at(main_hist_name);
            if ( !checkCuts(histogram.get()) ) { continue; }
            epsilon = (histogram->rh()->GetBinWidth(1))*0.5;
            cval    = branchVal(histogram->varName());
            filler  = std::min(cval,histogram->xmax() - epsilon);
            nom_w   = *(*m_nomWeightwLum);
            if ( process == "Data" ) {
              histogram->rh()->Fill(filler);
            }
            else {
              histogram->rh()->Fill(filler,m_lumscale*nom_w);
            }

            // all the weight systematics only for nominal tree
            if ( m_isNominal && m_doWeightSysts && process != "Data" ) {
              for ( auto const& weight : m_weightSysReaders ) {
                main_hist_name = channel+pdName+"_"+process+"_"+(j.at("name").get<std::string>());
                main_hist_name.append("_"+weight.first);
                histogram = m_aidaHistograms.at(main_hist_name);
                histogram->rh()->Fill(filler,(*(*weight.second))*m_lumscale);
              }
            }
          } // for the histogram name
          /////////////////////////////////////////////////////////////////////////////////////

        } // for the run period
      } // for each entry in the reader
    } // promt/fake trees
  } // for each tree
  ratios();
  return TL::STATUS::Good;
}

TL::STATUS AIDA::HistGenerator::ratios() {
  std::string outdirname(m_outDir->GetName());
  if ( outdirname != "AIDA_nominal" ) {
    return TL::STATUS::Good;
  }
  for ( const std::string& channel : m_topJson.at("channels") ) {
    for ( auto const& j : m_topJson.at("histograms") ) {
      std::string main_hist_name = channel+"_Ratio_"+(j.at("name").get<std::string>());
      int   inbins = j.at("bins").at(0).get<int>();
      float ixmin  = j.at("bins").at(1).get<float>();
      float ixmax  = j.at("bins").at(2).get<float>();
      auto hist = std::make_shared<AIDA::Histogram>(main_hist_name.c_str(),inbins,ixmin,ixmax);
      hist->rh()->Add(m_aidaHistograms.at(channel+"_Data_"+(j.at("name").get<std::string>()))->rh());
      auto temp_mc_hist = std::make_shared<TH1F>("tmh","tmh",inbins,ixmin,ixmax);
      temp_mc_hist->Sumw2();
      for ( const std::string& process : m_topJson.at("processes") ) {
        if ( process != "Data" ) {
          std::string mc_hist_name = channel+"_"+process+"_"+(j.at("name").get<std::string>());
          temp_mc_hist->Add(m_aidaHistograms.at(mc_hist_name)->rh());
        }
      }
      hist->rh()->Divide(temp_mc_hist.get());
      m_aidaHistograms.emplace(main_hist_name,hist);
    }
  }
  return TL::STATUS::Good;
}

TL::STATUS AIDA::HistGenerator::write() {
  auto dir_elel   = m_outDir->mkdir("elel");
  auto dir_elmu   = m_outDir->mkdir("elmu");
  auto dir_mumu   = m_outDir->mkdir("mumu");
  auto dir_elel15 = m_outDir->mkdir("elel2015");
  auto dir_elmu15 = m_outDir->mkdir("elmu2015");
  auto dir_mumu15 = m_outDir->mkdir("mumu2015");
  auto dir_elel16 = m_outDir->mkdir("elel2016");
  auto dir_elmu16 = m_outDir->mkdir("elmu2016");
  auto dir_mumu16 = m_outDir->mkdir("mumu2016");

  auto dfill = [](const std::string pfx, TDirectory* d, TDirectory* d15, TDirectory* d16, AIDA::Histogram* h) {
    if ( boost::algorithm::starts_with(h->name(),pfx) ) {
      if ( boost::algorithm::contains(h->name(),"2015_") ) {
        d15->cd();
      }
      else if ( boost::algorithm::contains(h->name(),"2016_") ) {
        d16->cd();
      }
      else {
        d->cd();
      }
      h->rh()->Write();
    }
  };

  for ( auto const& ah : m_aidaHistograms ) {
    dfill("elel",dir_elel,dir_elel15,dir_elel16,ah.second.get());
    dfill("elmu",dir_elmu,dir_elmu15,dir_elmu16,ah.second.get());
    dfill("mumu",dir_mumu,dir_mumu15,dir_mumu16,ah.second.get());
    gROOT->GetListOfFiles()->Remove(ah.second->rh());
  }
  m_outFile->Close();
  gROOT->GetListOfFiles()->Remove(m_outFile);
  return TL::STATUS::Good;
}

bool AIDA::HistGenerator::checkCuts(AIDA::Histogram* hist) {
  if ( !hist->hasCuts() ) { return true; }
  for ( auto const& cut : hist->greaterThanCuts() ) {
    if ( !(branchVal(cut.first) > cut.second) ) {
      return false;
    }
    else { continue; }
  }
  for ( auto const& cut : hist->greaterThanEqCuts() ) {
    if ( !(branchVal(cut.first) >= cut.second) ) {
      return false;
    }
    else { continue; }
  }
  for ( auto const& cut : hist->lessThanCuts() ) {
    if ( !(branchVal(cut.first) < cut.second) ) {
      return false;
    }
    else { continue; }
  }
  for ( auto const& cut : hist->lessThanEqCuts() ) {
    if ( !(branchVal(cut.first) <= cut.second) ) {
      return false;
    }
    else { continue; }
  }
  return true;
}
