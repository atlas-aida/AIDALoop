#!/bin/bash

# Relevant environment variables:
#   SCOUT=1   exit 99 after the first job is submitted
#   DRYRUN=1  don't actually submit anything
#   EMIT=1    print job scripts instead of submitting to PBS
#   FORCE=1   submit jobs even if they seem to have been run already
#   QUEUE     PBS queue to use

#todo- generalize more settings and/or grab from environment

RUNDIR=${RUNDIR:-"${ROOTCOREBIN}/.."}
TASK=${TASK:-"aidaloop"}
DEFAULT_PBS_QUEUE=${DEFUALT_PBS_QUEUE:-"long"}
QUEUE="${QUEUE:-$DEFAULT_PBS_QUEUE}"

TAG=${TAG:-"v4"}
PREFIXES=${PREFIXES:-"data tt_410000 wt_410015 wt_410016 ww_361081 wz_361067 zjets_361106 zjets_361378 zjets_361387 zjets_361396 zjets_361405 zjets_361414 zjets_361423 zjets_361432 zjets_361441 tt_410001 ww_361082 wz_361071 zjets_361107 zjets_361379 zjets_361388 zjets_361397 zjets_361406 zjets_361415 zjets_361424 zjets_361433 zjets_361442 tt_410002 ww_361600 wz_361083 zjets_361108 zjets_361380 zjets_361389 zjets_361398 zjets_361407 zjets_361416 zjets_361425 zjets_361434 zjets_361443 tt_410003 ww_361606 wz_361084 zjets_361372 zjets_361381 zjets_361390 zjets_361399 zjets_361408 zjets_361417 zjets_361426 zjets_361435 zz_361063 wwss_361069 wz_361601 zjets_361373 zjets_361382 zjets_361391 zjets_361400 zjets_361409 zjets_361418 zjets_361427 zjets_361436 zz_361072 wwss_361070 wz_361602 zjets_361374 zjets_361383 zjets_361392 zjets_361401 zjets_361410 zjets_361419 zjets_361428 zjets_361437 zz_361086 ww_361068 wz_361064 wz_361607 zjets_361375 zjets_361384 zjets_361393 zjets_361402 zjets_361411 zjets_361420 zjets_361429 zjets_361438 zz_361603 ww_361077 wz_361065 wz_361609 zjets_361376 zjets_361385 zjets_361394 zjets_361403 zjets_361412 zjets_361421 zjets_361430 zjets_361439 zz_361604 ww_361078 wz_361066 zjets_361065 zjets_361377 zjets_361386 zjets_361395 zjets_361404 zjets_361413 zjets_361422 zjets_361431 zjets_361440 zz_361610"}
SYSTEMATICS=${SYSTEMATICS:-"nominal EG_RESOLUTION_ALL__1down EG_RESOLUTION_ALL__1up EG_SCALE_ALL__1down EG_SCALE_ALL__1up JET_19NP_JET_BJES_Response__1down JET_19NP_JET_BJES_Response__1up JET_19NP_JET_EffectiveNP_1__1down JET_19NP_JET_EffectiveNP_1__1up JET_19NP_JET_EffectiveNP_2__1down JET_19NP_JET_EffectiveNP_2__1up JET_19NP_JET_EffectiveNP_3__1down JET_19NP_JET_EffectiveNP_3__1up JET_19NP_JET_EffectiveNP_4__1down JET_19NP_JET_EffectiveNP_4__1up JET_19NP_JET_EffectiveNP_5__1down JET_19NP_JET_EffectiveNP_5__1up JET_19NP_JET_EffectiveNP_6restTerm__1down JET_19NP_JET_EffectiveNP_6restTerm__1up JET_19NP_JET_EtaIntercalibration_Modelling__1down JET_19NP_JET_EtaIntercalibration_Modelling__1up JET_19NP_JET_EtaIntercalibration_NonClosure__1down JET_19NP_JET_EtaIntercalibration_NonClosure__1up JET_19NP_JET_EtaIntercalibration_TotalStat__1down JET_19NP_JET_EtaIntercalibration_TotalStat__1up JET_19NP_JET_Flavor_Composition__1down JET_19NP_JET_Flavor_Composition__1up JET_19NP_JET_Flavor_Response__1down JET_19NP_JET_Flavor_Response__1up JET_19NP_JET_Pileup_OffsetMu__1down JET_19NP_JET_Pileup_OffsetMu__1up JET_19NP_JET_Pileup_OffsetNPV__1down JET_19NP_JET_Pileup_OffsetNPV__1up JET_19NP_JET_Pileup_PtTerm__1down JET_19NP_JET_Pileup_PtTerm__1up JET_19NP_JET_Pileup_RhoTopology__1down JET_19NP_JET_Pileup_RhoTopology__1up JET_19NP_JET_PunchThrough_MC15__1down JET_19NP_JET_PunchThrough_MC15__1up JET_19NP_JET_SingleParticle_HighPt__1down JET_19NP_JET_SingleParticle_HighPt__1up JET_JER_SINGLE_NP__1up MET_SoftTrk_ResoPara MET_SoftTrk_ResoPerp MET_SoftTrk_ScaleDown MET_SoftTrk_ScaleUp MUONS_ID__1down MUONS_ID__1up MUONS_MS__1down MUONS_MS__1up MUONS_SCALE__1down MUONS_SCALE__1up"}

# Makes the PBS-compatible job script:
mkjob() {
  local name="$1"
  local output="$2"
  local conf_in="$3"
  local m_systematics="$4"
  local data_option=""
  if [[ $PREFIX == "data" ]]; then
    data_option=" --is-data"
  else
    data_option=""
  fi

  cat <<EOF
#!/bin/bash -f
#PBS -N $name
#PBS -j oe
#PBS -o $output.out

mkdir -p $(dirname "$output")
cd "$RUNDIR" && echo "Working in: \$PWD with job ${TASK} ${conf_in} ${m_systematics}"

setupATLAS
export X509_USER_PROXY=${X509_USER_PROXY}
lsetup rucio panda rcsetup fax
export analysisDir=${analysisDir}
export TL_INSTALL=\$analysisDir/TLWorkArea/install
export TL_BUILD=\$analysisDir/TLWorkArea/build
export TL_SOURCE=\$analysisDir/TLWorkArea/TopLoop
export AL_INSTALL=\$analysisDir/ALWorkArea/install
export AL_BUILD=\$analysisDir/ALWorkArea/build
export AL_SOURCE=\$analysisDir/ALWorkArea/AIDA-Loop
export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$TL_INSTALL/lib:$AL_INSTALL/lib
export PATH=\$PATH:$TL_INSTALL/bin:$AL_INSTALL/bin

runAIDA-Loop --loop-alg -o ${output}.root -t ${AL_SOURCE}/share/data/${TAG}/${PREFIX}.list -n ${m_systematics}${data_option}
EOF
}

submit() {
  local name="$1"
  local output_base="$2"
  local conf_in="$3"
  local m_systematics="$4"
  local output="$output_base/${conf_in}/${m_systematics}"

  if [[ -z "$FORCE" ]] && [[ -e "$output".root ]]; then
    echo "[INFO]  Already submitted: $name ($output)" >&2
  else
    echo "[INFO]  Submitting: $name ($output)"
    if [[ -z "$DRYRUN" ]]; then
      if [[ -z "$EMIT" ]]; then
        mkdir -p "$output_base"
        mkjob "$name" "$output" "$conf_in" "$m_systematics" \
          | qsub -q $QUEUE
      else
        mkjob "$name" "$output" "$conf_in" "$m_systematics"
      fi
    fi
  fi

  [[ -n "$SCOUT" ]] && exit 99
}

for PREFIX in $PREFIXES; do 
  if [[ $PREFIX == "data" ]]; then
    jobname="${TASK}.${PREFIX}"
    submit "$jobname" \
    "$RUNDIR/results/" \
    "$PREFIX" \
    "nominal"
    sleep 2
  else
    for SYSTEMATIC in $SYSTEMATICS; do
      jobname="${TASK}.${PREFIX}.${SYSTEMATIC}"
      submit "$jobname" \
      "$RUNDIR/results/" \
      "$PREFIX" \
      "$SYSTEMATIC"
      #avoid overloading batch manager
      sleep 2
    done
  fi
done
