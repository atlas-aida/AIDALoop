#!/usr/bin/env python2


## This python script runs AIDA::LoopAlg on all of the
## directories inside `datadir` and `mcdir`.
## the output is a ROOT file for each directory, named with
## simply the DSID of the folder which was analyzed.
## douglas.davis@cern.ch

import os
import subprocess

# location of the v1b ntuples
datadir = '/var/phy/project/hep/atlas/users/drd25/top/v2dntuples/Data'
mcdir   = '/var/phy/project/hep/atlas/users/drd25/top/v1bntuples/MC'


##################
## nominal runs ##
##################


## run data
############
listofdirs = os.walk(datadir).next()[1]
dsids_and_files = { i.split('.')[2] : datadir+'/'+'.'.join(i.split('.')[0:3])+'.*' for i in listofdirs }

for dsid, fp in dsids_and_files.iteritems():
    command = ''.join(['runAIDA-Loop --loop-alg --is-data -a ',fp,' -o ',dsid,'.root'])
    print command
    subprocess.call(command,shell=True)


## run MC
##########
#listofdirs = os.walk(mcdir).next()[1]
#dsids_and_files = { i.split('.')[2] : mcdir+'/'+'.'.join(i.split('.')[0:3])+'.*' for i in listofdirs }

#for dsid, fp in dsids_and_files.iteritems():
#    command = ''.join(['runAIDA-Loop --loop-alg -a ',fp,' -o ',dsid,'.root'])
#    print command
#    subprocess.call(command,shell=True)


#############################
## future non-nominal runs ##
#############################

## ....
