# list of the dsids we are using in AIDA
# contact: ddavis@cern.ch

table = {
    "ttbar_PowPy8"     : [410501] ,
    "ttbar_PowPy8_dil" : [410503] ,
    "ttbar_PowPy6"     : [410000] ,
    "Wt_PowPy6"        : [410015,410016] ,
    "Zjets_S21"        : [i for i in range(361372,361444)] ,
    "Zjets_S22"        : [i for i in range(363361,363411)] + [i for i in range(363102,363123)] ,
    "Zjets_S221"       : [i for i in range(364100,364142)] ,
    "Wjets_S22"        : [i for i in range(363436,363484)] + [i for i in range(363331,363354)] ,
    "Wjets_S221"       : [i for i in range(364156,364198)] ,
    "Diboson_PowPy8"   : [361601, 361603, 361604, 361607, 361609, 361610, 361611] ,
    "WW_PowPy8"        : [361600, 361606] ,
    "WW_dedicated"     : [361600, 361078] ,
    "Diboson_dedicated": [361601, 361607, 361603, 361604, 361610]
}
