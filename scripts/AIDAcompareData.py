#!/usr/bin/env python2
import sys
file1 = sys.argv[1]
file2 = sys.argv[2]
import ROOT
from ROOT import kBlack, kGray, kOrange, kGreen, kBlue, kBlack, kWhite, kRed, kAzure, kGray
from AIDAplotUtils import compareHists

ROOT.gROOT.SetBatch(True)

file1 = ROOT.TFile(file1,'read')
file2 = ROOT.TFile(file2,'read')

histogram_types = ['njets',
                   'njets_3bins',
                   'njets_3bins_l30met',
                   'njets_3bins_ge30met',
                   'met',
                   'met_0j',
                   'met_1j',
                   'met_ge2j']
xtitles = ['N_{\\mathrm{jets}}',
           'N_{\\mathrm{jets}}',
           'N_{\\mathrm{jets}}\,(E_T^{\\mathrm{miss}}<30\,\\mathrm{GeV})',
           'N_{\\mathrm{jets}}\,(E_T^{\\mathrm{miss}}\geq30\,\\mathrm{GeV})',
           'E_T^{\\mathrm{miss}}\,(\\mathrm{GeV})',
           'E_T^{\mathrm{miss}}\,(N_{\mathrm{jets}} = 0)\,(\\mathrm{GeV})',
           'E_T^{\mathrm{miss}}\,(N_{\mathrm{jets}} = 1)\,(\\mathrm{GeV})',
           'E_T^{\\mathrm{miss}}\,(N_{\\mathrm{jets}} \\geq 2)\,\mathrm{(GeV)}']
ytitles = ['Events/bin',
           'Events/bin',
           'Events/bin',
           'Events/bin',
           'Events/10 GeV',
           'Events/10 GeV',
           'Events/10 GeV',
           'Events/10 GeV']
cbinlabels = [None,['0','1','#geq 2'],['0','1','#geq 2'],['0','1','#geq 2'],
                   None,None,None,None]

rt = '2.3.9/2.4.6'

histograms1 = { ht : file1.Get('data_'+ht) for ht in histogram_types }
histograms2 = { ht : file2.Get('data_'+ht) for ht in histogram_types }

for ht, xt, yt, cbl  in zip(histogram_types,xtitles,ytitles,cbinlabels):
    a = compareHists(ht,histograms1[ht],histograms2[ht],'2.3.49 Data','2.4.6 Data',kRed,kBlack,
                     xtitle=xt,ytitle=yt,ratiotitle=rt)
    a.draw(manualmin=0,custombinlabels=cbl)
