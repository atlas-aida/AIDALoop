#!/usr/bin/env python2

import sys
import argparse
import os
import copy
parser = argparse.ArgumentParser(description='AIDA plotting utility')
parser.add_argument('-i','--in-file',help='input ROOT file containing histograms',required=True,type=str)
parser.add_argument('-o','--out-dir',help='output directory to write plots to',   required=True,type=str)
parser.add_argument('-p','--run-period',help='run period: "2015", "2016", or leave unspecified for both',
                    required=False,type=str,default='')
parser.add_argument('-c','--channel',help='lepton flavor channel: "elel","elmu","mumu"',required=True, type=str)
parser.add_argument('-l','--lumi',help='Luminosity to print on plot in /fb',
                    required=False,type=str,default='XX')
args = vars(parser.parse_args())
if not os.path.exists(args['out_dir']):
    os.mkdir(args['out_dir'])

import ROOT
from ROOT import kBlack, kGray, kOrange, kGreen, kBlue, kBlack, kWhite, kRed, kAzure, kGray
from AIDALoop.plot_utils import stackedHistogram

ROOT.gROOT.SetBatch(True)

infile = ROOT.TFile(args['in_file'],'read')
outdir = args['out_dir']

mc_processes = ['Diboson','Fake','WW','Wt','Zjets','ttbar']
mc_colors    = [kBlack,kGray,kAzure+2,kGreen+2,kOrange+1,kWhite]
mcl_colors   = [kBlack,kBlack,kBlack,kBlack,kBlack,kBlack]
procs_cols   = { proc : col for proc, col in zip(mc_processes,mc_colors) }
xaxes        = ['llpT','llpT_0j','met_0j','met_ge2j',
                'njets','njets_3bins','njets_3bins_l30met','njets_3bins_ge30met']
axistitles   = [['Leading lepton #it{p}_{T} (GeV)','GeV',False,False],
                ['Leading lepton #it{p}_{T} (N_{jets} = 0) (GeV)','GeV',False,False],
                ['E^{miss}_{T} (N_{jets} = 0) (GeV)','GeV',False,False],
                ['E^{miss}_{T} (N_{jets} #geq 2) (GeV)','GeV',True,False],
                ['Jet Multiplicity','',True,True],
                ['Jet Multiplicity','',True,True],
                ['Jet Multiplicity (#it{E}_{T}^{miss} < 30 GeV)','',True,True],
                ['Jet Multiplicity (#it{E}_{T}^{miss} #geq 30 GeV)','',True,True]]

channel = args['channel']
period = args['run_period']
topdir = 'AIDA_nominal'
histpref = topdir+'/'+channel+period+'/'+channel+period
histograms = { pt : { xt : infile.Get(histpref+'_'+pt+'_'+xt)
                      for xt in xaxes } for pt in mc_processes }
histograms['data']  = { xt : infile.Get(histpref+'_Data_'+xt)  for xt in xaxes }
histograms['ratio'] = { xt : infile.Get(histpref+'_Ratio_'+xt) for xt in xaxes }

#histograms['WZ/ZZ'] = {}
#for xa in xaxes:
#    histograms['WZ/ZZ'][xa] = histograms['WZ'][xa]
#    histograms['WZ/ZZ'][xa].Add(histograms['ZZ'][xa])

mc_processes = ['Diboson','Fake','WW','Wt','Zjets','ttbar']
mc_colors    = [kBlack,kGray,kAzure+2,kGreen+2,kOrange+1,kWhite]
mcl_colors   = [kBlack,kBlack,kBlack,kBlack,kBlack,kBlack]
histograms_lists = { x : [histograms[pt][x] for pt in mc_processes] for x in xaxes }

## loop does all the plotting!
for xaxis, title in zip(xaxes,axistitles):
    current = stackedHistogram(xaxis+'Plot',
                               histograms_lists[xaxis],
                               histograms['data'][xaxis],
                               histograms['ratio'][xaxis],
                               mc_colors,lcols=mcl_colors,outdir=outdir,lumi=args['lumi'],logging=title[2])
    current.draw(xtitle=title[0],yunit=title[1],logscale=title[2],njetbins=title[3])
