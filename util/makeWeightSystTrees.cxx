#include <iostream>
#include <memory>

#include <TFile.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>

#include <TopLoop/Core/Utils.h>

template<typename T>
std::shared_ptr<TTreeReaderValue<T>> setupTreeVar(TTreeReader& reader, const char* name);

template<typename T>
T get(std::shared_ptr<TTreeReaderValue<T>> p);

int main(int argc, char *argv[]) {
  if ( argc < 2 ) {
    std::cout << "Give file name." << std::endl;
    return 0;
  }

  int   m_runNumber;
  bool  m_SS;
  bool  m_elel;
  bool  m_elmu;
  bool  m_mumu;
  float m_met;
  float m_njets;
  float m_nbjets;
  float m_llpT;
  float m_lleta;
  float m_llpdg;
  float m_sllpT;
  float m_slleta;
  float m_sllpdg;
  float m_ljpT;
  float m_ljeta;
  float m_nomWeightwLum;
  std::map<std::string,float> m_eventWeight;

  TFile *infile = new TFile(argv[1],"UPDATE");
  TTree *intree = dynamic_cast<TTree*>(infile->Get("AIDA_nominal"));
  TTreeReader reader(intree);

  auto weightMap = setupTreeVar<std::map<std::string,float>>(reader,"eventWeight");
  auto runNumber = setupTreeVar<int>  (reader,"runNumber");
  auto SS        = setupTreeVar<bool> (reader,"SS");
  auto elel      = setupTreeVar<bool> (reader,"elel");
  auto elmu      = setupTreeVar<bool> (reader,"elmu");
  auto mumu      = setupTreeVar<bool> (reader,"mumu");
  auto met       = setupTreeVar<float>(reader,"met");
  auto njets     = setupTreeVar<float>(reader,"njets");
  auto nbjets    = setupTreeVar<float>(reader,"nbjets");
  auto llpdg     = setupTreeVar<float>(reader,"llpdg");
  auto llpT      = setupTreeVar<float>(reader,"llpT");
  auto lleta     = setupTreeVar<float>(reader,"lleta");
  auto sllpdg    = setupTreeVar<float>(reader,"sllpdg");
  auto sllpT     = setupTreeVar<float>(reader,"sllpT");
  auto slleta    = setupTreeVar<float>(reader,"slleta");
  auto ljpT      = setupTreeVar<float>(reader,"ljpT");
  auto ljeta     = setupTreeVar<float>(reader,"ljeta");

  std::map<std::string,TTree*> newTrees;

  reader.SetEntry(0);
  for ( auto const& entry : get(weightMap) ) {
    if ( entry.first == "nominal" ) continue;
    auto nt = new TTree(("AIDA_"+entry.first).c_str(),("AIDA_"+entry.first).c_str());
    nt->Branch("runNumber",     &m_runNumber);
    nt->Branch("SS",            &m_SS);
    nt->Branch("elel",          &m_elel);
    nt->Branch("elmu",          &m_elmu);
    nt->Branch("mumu",          &m_mumu);
    nt->Branch("met",           &m_met);
    nt->Branch("njets",         &m_njets);
    nt->Branch("nbjets",        &m_nbjets);
    nt->Branch("llpdg",         &m_llpdg);
    nt->Branch("llpT",          &m_llpT);
    nt->Branch("lleta",         &m_lleta);
    nt->Branch("sllpdg",        &m_sllpdg);
    nt->Branch("sllpT",         &m_sllpT);
    nt->Branch("slleta",        &m_slleta);
    nt->Branch("ljpT",          &m_ljpT);
    nt->Branch("ljeta",         &m_ljeta);
    nt->Branch("nomWeightwLum", &m_nomWeightwLum);
    nt->Branch("eventWeight",   &m_eventWeight);
    newTrees.emplace(entry.first,nt);
  }

  for ( long i = 0; i < reader.GetEntries(true); ++i ) {
    reader.SetEntry(i);
    m_runNumber = get(runNumber);
    m_SS        = get(SS);
    m_elel      = get(elel);
    m_elmu      = get(elmu);
    m_mumu      = get(mumu);
    m_met       = get(met);
    m_njets     = get(njets);
    m_nbjets    = get(nbjets);
    m_llpdg     = get(llpdg);
    m_llpT      = get(llpT);
    m_lleta     = get(lleta);
    m_sllpdg    = get(sllpdg);
    m_sllpT     = get(sllpT);
    m_slleta    = get(slleta);
    m_ljpT      = get(ljpT);
    m_ljeta     = get(ljeta);
    for ( auto const& iweight : get(weightMap) ) {
      if ( iweight.first == "nominal" ) continue;
      m_eventWeight.clear();
      m_nomWeightwLum = iweight.second;
      m_eventWeight.emplace(iweight.first,iweight.second);
      newTrees.at(iweight.first)->Fill();
    }
  }

  for ( auto& tree : newTrees ) {
    tree.second->Write();
  }

  infile->Close();
  return 0;
}

template<typename T>
std::shared_ptr<TTreeReaderValue<T>> setupTreeVar(TTreeReader& reader, const char* name) {
  if ( reader.GetTree()->GetListOfBranches()->FindObject(name) != nullptr ) {
    return std::make_shared<TTreeReaderValue<T>>(reader,name);
  }
  else {
    TL::Warning(__FUNCTION__,name,"branch not found in tree");
    return nullptr;
  }
}

template<typename T>
T get(std::shared_ptr<TTreeReaderValue<T>> p) {
  return *(*p);
}
