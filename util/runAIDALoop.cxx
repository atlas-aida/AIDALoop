// TL and AL
#include <AIDALoop/LoopAlg.h>
#include <AIDALoop/ParticleLevelLoop.h>
#include <AIDALoop/HistGenerator.h>
#include <TopLoop/Core/Job.h>
#include <TopLoop/Core/FileManager.h>

// RootCore
#include <PathResolver/PathResolver.h>

// C++
#include <iostream>
#include <memory>

// Boost
#include <boost/program_options.hpp>

int main(int argc, char *argv[]) {
  namespace bpo = boost::program_options;
  bpo::options_description opts("AIDA-Loop steering program");
  opts.add_options()
    ("help,h","Display help message")
    ("loop-alg",
     "Run the loop algorithm (requires one of the following: -d -a -t, and -o")
    ("is-data","Flag to tell algorithm it's analyzing data")
    ("particle-level","Flag to tell algorithm to loop over particle level tree")
    ("feed-dir,d",bpo::value<std::string>(),
     "flag to point the algorithm to a directory containing .root files to loop over.")
    ("feed-dir-all,a",bpo::value<std::string>(),
     "See --feed-dir, but take _all_ files in the directory (e.g. will take file.root.2)")
    ("feed-txt,t",bpo::value<std::string>(),
     "flag to input a text file containing a list of ROOT files")
    ("tree-name,n",bpo::value<std::string>()->default_value("nominal"),
     "name of input tree (if not 'nominal')")
    ("out-name,o",bpo::value<std::string>(),
     "output file name (always required)")
    ("no-ttrv-warning","Flag to tell algorithm to not display tree variable warning")
    ("full-output","Flag to tell algorithm to include more output (more disk space!)")
    ("do-generator-weights","Run the on the fly generator weights")
    ("only-osof","only opposite sign opposite flavor final states are stored")
    ("verbose","Print all INFO messages (some are ALWAYS on, some off by default)")
    ("override-outtree-name",bpo::value<std::string>(),"override the output tree name");

  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(argc,argv,opts),vm);
  bpo::notify(vm);

  if ( vm.count("help") ) {
    std::cout << opts << std::endl;
    return 0;
  }

  if ( !vm.count("loop-alg") && !vm.count("gen-hists") ) {
    std::cout << "Must run either the loog algorithm (--loog-alg),\n";
    std::cout << "or the histogram generator (--gen-hists)!\n";
    std::cout << opts << std::endl;
    return 0;
  }

  if ( !vm.count("out-name") ) {
    std::cout << "Must give an output file name! (-o)" << std::endl;
    std::cout << opts << std::endl;
    return 0;
  }

  // For running the AIDA::LoopAlg algorithm
  // _____________________________________________________________________________
  if ( vm.count("loop-alg") ) {
    if ( !vm.count("feed-dir") && !vm.count("feed-dir-all") && !vm.count("feed-txt") ) {
      std::cout << "To run the loop algorithm you must supply one of the following:\n";
      std::cout << " -d, -a, -t " << std::endl;
      std::cout << opts << std::endl;
      return 0;
    }
    else {
      //auto partAlg = std::make_unique<AIDA::ParticleLevelLoop>();
      auto loopAlg = std::make_unique<AIDA::LoopAlg>();
      if ( vm.count("verbose") ) {
        //partAlg->setVerboseOn();
        loopAlg->setVerboseOn();
      }
      if ( vm.count("override-outtree-name") ) {
        loopAlg->setOverrideOutTreeName(vm["override-outtree-name"].as<std::string>());
      }
      std::shared_ptr<TL::FileManager> fm;
      if ( vm.count("particle-level") ) {
	//partAlg->setOutfileName(vm["out-name"].as<std::string>());
	//fm = partAlg->fileManager();
      }
      else {
	loopAlg->setOutfileName(vm["out-name"].as<std::string>(),vm.count("full-output"));
	fm = loopAlg->fileManager();
      }
      if ( vm.count("do-generator-weights") ) {
        TL::Info("runAIDALoop","Processing generator weights!");
        loopAlg->doGeneratorWeights();
      }
      if ( vm.count("only-osof") ) {
        TL::Info("runAIDALoop","Only outputting opposite sign emu events");
        loopAlg->onlyOSOF();
      }
      //this needs to come before FileManager::feedX()
      if (vm.count("tree-name") ) {
	std::string treename = vm["tree-name"].as<std::string>();
	loopAlg->fileManager()->setTreeName(treename);
	//for now we assume you're running systematics if you use a tree other than
	//'nominal'
	if ( treename != "nominal" ) loopAlg->setIsSystematic();
      }

      if ( vm.count("feed-dir") ) {
	fm->feedDir(vm["feed-dir"].as<std::string>());
      }

      if ( vm.count("feed-dir-all") ) {
	fm->feedDir(vm["feed-dir-all"].as<std::string>(),true);
      }

      if ( vm.count("feed-txt") ) {
	fm->feedTxt(vm["feed-txt"].as<std::string>());
      }
      if ( fm->fileNames().empty() ) {
	std::cout << "Empty dataset! Exiting" << std::endl;
	return 0;
      }

      if ( vm.count("is-data") ) {
	loopAlg->setIsData();
      }
      if ( vm.count("no-ttrv-warning") ) {
        loopAlg->turnOffTTRVWarning();
      }

      if ( vm.count("particle-level") ) {
	//TL::Job job(partAlg.get());
	//job.setParticleLevelRun();
	//job.run();
      }
      else {
	TL::Job job(loopAlg.get());
	job.run();
      }
      return 0;
    }
  }

  return 0;
}
