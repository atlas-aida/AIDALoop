/** @file  HistGenerator.h
 *  @brief AIDA::HistGenerator class header
 *  @class AIDA::HistGenerator
 *  @brief A class to handle the generation
 *         of ROOT histograms
 *
 *  This class parses the AIDA::LoopAlg output
 *  files to generate histograms defined from
 *  a json configuration file; separating
 *  the different processes accordingly.
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef AIDA_HistGenerator_h
#define AIDA_HistGenerator_h

// C++
#include <map>
#include <memory>
#include <vector>
#include <tuple>

// ROOT
#include "TH1F.h"
#include "TFile.h"
#include "TTreeReaderValue.h"

// TopLoop
#include "TopLoop/Core/Utils.h"
#include "TopLoop/Core/SampleMetaSvc.h"
#include "TopLoop/json/json.hpp"

// ROOT
class TTree;
class TChain;
class TTreeReader;

namespace AIDA {

  class Histogram {
  private:
    std::string                 m_name;
    unsigned int                m_nbins;
    float                       m_xmin;
    float                       m_xmax;
    bool                        m_hasCuts;

    std::string                 m_varName;
    TH1F*                       m_rh;

    std::map<std::string,float> m_greaterThanCuts;
    std::map<std::string,float> m_greaterThanEqCuts;
    std::map<std::string,float> m_lessThanCuts;
    std::map<std::string,float> m_lessThanEqCuts;

  public:
    Histogram() {} // unused default constructor
    Histogram(const std::string hname, const int nbins, const float xmin, const float xmax)
      : m_name(hname)  ,
        m_nbins(nbins) ,
        m_xmin(xmin)   ,
        m_xmax(xmax)   ,
        m_hasCuts(false)
    {
      m_rh = new TH1F(hname.c_str(),hname.c_str(),nbins,xmin,xmax);
      m_rh->Sumw2();
    }
    virtual ~Histogram() {}

    const std::string& name() const { return m_name; }

    TH1F* rh() { return m_rh; }
    const TH1F* rh() const { return m_rh; }

    unsigned int nbins() const { return m_nbins; }
    float        xmin() const  { return m_xmin;  }
    float        xmax() const  { return m_xmax;  }

    void hasCuts(bool hc) { m_hasCuts = hc;   }
    bool hasCuts() const  { return m_hasCuts; }

    void setVar(const std::string vname) { m_varName = vname; }
    const std::string& varName() const { return m_varName; }

    void addGreaterThanCut(const std::string s, const float v)   { m_greaterThanCuts.emplace(s,v);   }
    void addGreaterThanEqCut(const std::string s, const float v) { m_greaterThanEqCuts.emplace(s,v); }
    void addLessThanCut(const std::string s, const float v)      { m_lessThanCuts.emplace(s,v);      }
    void addLessThanEqCut(const std::string s, const float v)    { m_lessThanEqCuts.emplace(s,v);    }

    const std::map<std::string,float>& greaterThanCuts()   const { return m_greaterThanCuts;   }
    const std::map<std::string,float>& greaterThanEqCuts() const { return m_greaterThanEqCuts; }
    const std::map<std::string,float>& lessThanCuts()      const { return m_lessThanCuts;      }
    const std::map<std::string,float>& lessThanEqCuts()    const { return m_lessThanEqCuts;    }

    std::map<std::string,float>& greaterThanCuts()   { return m_greaterThanCuts;   }
    std::map<std::string,float>& greaterThanEqCuts() { return m_greaterThanEqCuts; }
    std::map<std::string,float>& lessThanCuts()      { return m_lessThanCuts;      }
    std::map<std::string,float>& lessThanEqCuts()    { return m_lessThanEqCuts;    }
  };

  class HistGenerator {
  private:
    TFile*                                                          m_outFile;
    TDirectory*                                                     m_outDir;
    std::string                                                     m_treename;
    std::string                                                     m_treename_f;
    bool                                                            m_isNominal;
    bool                                                            m_doWeightSysts;
    bool                                                            m_doSS;
    std::vector<std::tuple<TTree*,TTree*,TTree*>>                   m_trees;
    nlohmann::json                                                  m_topJson;
    std::shared_ptr<TTreeReader>                                    m_reader;
    std::shared_ptr<TTreeReader>                                    m_metaReader;
    std::map<std::string,std::shared_ptr<TTreeReaderValue<float>>>  m_variables;
    std::map<std::string,std::shared_ptr<AIDA::Histogram>>          m_aidaHistograms;
    std::shared_ptr<TTreeReaderValue<int>>                          m_dsid;
    std::shared_ptr<TTreeReaderValue<int>>                          m_runNumber;
    std::shared_ptr<TTreeReaderValue<bool>>                         m_SS;
    std::shared_ptr<TTreeReaderValue<bool>>                         m_elel;
    std::shared_ptr<TTreeReaderValue<bool>>                         m_mumu;
    std::shared_ptr<TTreeReaderValue<bool>>                         m_elmu;
    std::shared_ptr<TTreeReaderValue<float>>                        m_nomWeightwLum;
    std::map<std::string,std::shared_ptr<TTreeReaderValue<float>>>  m_weightSysReaders;
    std::shared_ptr<TL::SampleMetaSvc>                              m_sampleMetaSvc;
    float                                                           m_lumscale;

  public:
    HistGenerator();
    HistGenerator(const char* outfilename, std::string treename,
                  const char* textfilename, const float lumscale,
                  const bool doWeightSysts = false, bool doSS = false);
    virtual ~HistGenerator();

    void parseJson(const char* jsonfilename);

    TL::STATUS fill();
    TL::STATUS ratios();
    TL::STATUS write();

    bool checkCuts(AIDA::Histogram* hist);
    float branchVal(const std::string var) const { return *(*(m_variables.at(var))); }

    template <class T>
    T get(std::shared_ptr<TTreeReaderValue<T>> ptr) { return *(*ptr); }
  };

}

#endif
