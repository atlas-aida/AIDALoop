/** @file  LoopAlg.h
 *  @brief AIDA::LoopAlg class header
 *  @class AIDA::LoopAlg
 *  @brief A class which is a TopLoop
 *         based algorithm for AIDA.
 *
 *  This class is a TopLoop algorithm for
 *  reading the output of the AnalysisTop
 *  ntuples, and generating output for
 *  AIDA group use (histogramming and fitting).
 *
 *  @author Douglas Davis < douglas.davis@cern.ch >
 *  @author Kevin Finelli < kevin.finelli@cern.ch >
 */

#ifndef AIDA_LoopAlg_h
#define AIDA_LoopAlg_h

// TL
#include <TopLoop/Core/AlgBase.h>
#include <TopLoop/Core/SampleMetaSvc.h>
#include <TopLoop/EDM/FinalState.h>

// ROOT
class TFile;
class TTree;

// TopDataPreparation RootCore package
class SampleXsection;

namespace AIDA {

  class LoopAlg : public TL::AlgBase {
  private:
    float m_dataLumi;
    long  m_totalEntries;
    float m_totalWeights;
    int   m_eventCounter;
    std::vector<float> m_weightsVecTL;
    std::vector<std::string> m_genWnames;

    std::string           m_outfileName;
    std::string           m_outtreeName;
    bool                  m_overrideOTN;
    bool                  m_doGeneratorWeights;
    bool                  m_fullOutput;
    bool                  m_OS_only; // opposite sign only
    bool                  m_OF_only; // opposite flavor only
    TFile*                m_outputFile;
    TTree*                m_AIDAflatTree;
    TTree*                m_AIDAflatTreeMCFakes;
    TTree*                m_AIDAmetaTree;
    const SampleXsection* m_sampleXsection;

    // our additional top-xaod flat tree accessors {
    // these are variables not output by default
    std::shared_ptr<TTRV_vec_char>  el_truthMatched;
    std::shared_ptr<TTRV_vec_int>   el_true_pdg;
    std::shared_ptr<TTRV_vec_float> el_true_pt;
    std::shared_ptr<TTRV_vec_float> el_true_eta;
    std::shared_ptr<TTRV_vec_char>  mu_truthMatched;
    std::shared_ptr<TTRV_vec_int>   mu_true_pdg;
    std::shared_ptr<TTRV_vec_float> mu_true_pt;
    std::shared_ptr<TTRV_vec_float> mu_true_eta;
    std::shared_ptr<TTRV_float>     met_sumet;
    // }

    // meta information {
    int           m_runNumber;
    int           m_dsid;
    std::string   m_initialState;
    std::string   m_generator;
    std::string   m_sampleType;
    // }

    // basic final state information {
    bool          m_SS;
    bool          m_mumu;
    bool          m_elel;
    bool          m_elmu;
    float         m_eventMass;
    float         m_dileptonMass;
    float         m_met;
    float         m_njets;
    float         m_nbjets_manual;
    float         m_nbjets_AT;
    float         m_ht;

    float         m_dphileps;
    float         m_dRleps;
    float         m_dphimetll;
    float         m_dphimetsll;

    // }

    // Lepton and Jet information {
    float         m_llpT;   // leading lepton
    float         m_lleta;
    float         m_llphi;
    float         m_llpdg;

    float         m_sllpT;  // subleading lepton
    float         m_slleta;
    float         m_sllphi;
    float         m_sllpdg;

    float         m_elpT;
    float         m_mupT;
    float         m_eleta;
    float         m_mueta;

    float         m_ljpT;   // leading jet
    float         m_ljeta;
    float         m_ljphi;
    float         m_lje;

    float         m_sljpT;  // subleading jet
    float         m_sljeta;
    float         m_sljphi;
    float         m_slje;
    // }

    // weight equal to [data]fb^-1 / MC_lumi
    float m_lumiWeight;

    // the first entry in the eventWeightList
    float m_nomWeight;

    // the lumiWeight * nominal weight
    float m_nomWeightwLum;

    // product of MC generator weight and (nominal) lepton, tagging,
    // and pileup scale factors. key: weightSys name, val: the final
    // product including the respective weight.
    std::map<std::string,float> m_weightBranchMap;

    // all the extra MC generator weights
    std::map<std::string,float> m_genWeightsBranchMap;

    TL::EDM::FinalState m_finalState;

  public:
    LoopAlg();
    virtual ~LoopAlg();

    void setOutfileName(const std::string& outname, bool full_output = false);
    void setOverrideOutTreeName(const std::string& outtreename);
    void doGeneratorWeights();
    void onlyOSOF();

    TL::STATUS init()        final override;
    TL::STATUS setupOutput() final override;
    TL::STATUS execute()     final override;
    TL::STATUS finish()      final override;

    void fillObjects();
    void fillWeights();
    void fillFlatTree();
  };
}

inline void AIDA::LoopAlg::setOutfileName(const std::string& outname, bool full_output) {
  m_outfileName = outname;
  m_fullOutput  = full_output;
}

inline void AIDA::LoopAlg::setOverrideOutTreeName(const std::string& outtreename) {
  m_outtreeName = outtreename;
  m_overrideOTN = true;
}

inline void AIDA::LoopAlg::doGeneratorWeights() {
  m_doGeneratorWeights = true;
}

inline void AIDA::LoopAlg::onlyOSOF() {
  m_OF_only = true;
  m_OS_only = true;
}

namespace AIDA {
  template <class T>
  void set_zero() {}

  template <class T>
  void set_zero(T& val) {
    val = 0.0;
  }

  template <class T, class... Ts>
  void set_zero(T& val, Ts&... vals) {
    set_zero(val);
    set_zero(vals...);
  }
}

#endif
