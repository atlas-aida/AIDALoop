/** @file  ParticleLevelLoop.h
 *  @brief AIDA::ParticleLevelLoop class header
 *  @class AIDA::ParticleLevelLoop
 *  @brief Particle level analysis loop algorithm.
 *
 *  This class is a TopLoop algorithm for analyzing the AnalysisTop
 *  particle level truth information.
 *
 *  @author Douglas Davis < douglas.davis@cern.ch >
 *  @author Kevin Finelli < kevin.finelli@cern.ch >
 */

#ifndef AIDA_ParticleLevelLoop_h
#define AIDA_ParticleLevelLoop_h

// TL
#include <TopLoop/Core/AlgBase.h>
#include <TopLoop/EDM/FinalState.h>

// ROOT
class TFile;
class TTree;

// RootCore
class SampleXsection;

namespace AIDA {

  class ParticleLevelLoop : public TL::AlgBase {
  private:
    long m_totalEntries;
    int  m_eventCounter;
    const SampleXsection* m_sampleXsection;

    TL::EDM::FinalState m_finalState;

  public:
    ParticleLevelLoop();
    virtual ~ParticleLevelLoop();

    virtual TL::STATUS init();
    virtual TL::STATUS setupOutput();
    virtual TL::STATUS execute();
    virtual TL::STATUS finish();

    void fillObjects();

  };

}

#endif
