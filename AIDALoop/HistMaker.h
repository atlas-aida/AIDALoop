/** @file  HistMaker.h
 *  @brief AIDA::HistMaker class header
 *  @class AIDA::HistMaker
 *  @brief A class to handle the generation
 *         of ROOT histograms
 *
 *  This class parses the AIDA::LoopAlg output
 *  files to generate histograms, separating
 *  the different processes accordingly.
 *
 *  @author Douglas Davis < douglas.davis@cern.ch >
 */

#ifndef AIDA_HistMaker_h
#define AIDA_HistMaker_h

// C++
#include <map>
#include <vector>
#include <tuple>
#include <algorithm>

// Boost
#include <boost/algorithm/string/predicate.hpp>

// ROOT
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"

// TopLoop
#include "TopLoop/Core/Utils.h"
#include "TopLoop/Core/SampleMetaSvc.h"

// ROOT
class TTree;

namespace AIDA {
  class HistMaker {
  private:

    std::string m_treeName;

    //! A list of parent processes
    /*!
      The different processes which we separate
      dilepton final states into (e.g. tt, ww, etc.)
    */
    std::vector<std::string> m_processes;

    //! A list of lepton channels
    /*!
      The different dilepton channels
      we sort into, ee, uu, eu.
    */
    std::vector<std::string> m_channels;

    //! 1d Histogram properties container
    /*! 
      tabs stands for "types and binnings"
      Key:   A kinematic variable.
      Value: A tuple of binning information
      (nbins, xmin, xmax, epsilon). The
      epsilon is for overflow handling and
      must be smaller than the bin width.
    */
    std::map<std::string, std::tuple<unsigned int,float,float,float> > m_tabs_1d;

    //! Histogram properties container (for 2d histograms)
    /*! 
      see m_tabs_1d
    */
    std::map<std::string, std::tuple<unsigned int,float,float,float,
                                     unsigned int,float,float,float> > m_tabs_2d;

    //! Histogram container for 1d distributions
    /*!
      A container for all of the histograms
      that will be written to the output file.
      Key:   The histogram title
      Value: A pointer to the ROOT Histogram.
    */
    std::map<std::string,TH1F*> m_histograms_1d;

    //! Histogram container for 2d distributions
    /*!
      see m_histograms_1d
    */
    std::map<std::string,TH2F*> m_histograms_2d;

    //! scale factor for luminosity
    /*!
      AIDA::LoopAlg outputs scale to 1/fb,
      this is to scale to larger integrated
      luminosities
    */
    float m_lumiScale;

    std::vector<TFile*> m_files;
    std::vector<TTree*> m_metaTrees;
    std::vector<TTree*> m_flatTrees;

    template <class T1, class T2>
    float filler(const T1 val, const T2& tab) const {
      float overflow = std::get<2>(tab) - std::get<3>(tab);
      return std::min((float)val,overflow);
    }

    std::shared_ptr<TL::SampleMetaSvc> m_sampleMetaSvc;

  public:
    HistMaker();
    virtual ~HistMaker();

    void setTreeName(const std::string& name);
    void setLumiScale(const float val);
    void addFiles(const std::initializer_list<std::string>& fs);
    void addFiles(const std::vector<std::string>& fs);
    void generate();
    void makeRatios();
    template <class T> void loopToFill(TDirectory* outDir, const T& container);
    void writeOut(const char* filename);
  };
}

inline void AIDA::HistMaker::setTreeName(const std::string& name) { m_treeName = name; }
inline void AIDA::HistMaker::setLumiScale(const float val)        { m_lumiScale = val; }

template <class T> void AIDA::HistMaker::loopToFill(TDirectory* outDir, const T& container) {
  auto ee_dir   = outDir->GetDirectory("ee");
  auto uu_dir   = outDir->GetDirectory("uu");
  auto eu_dir   = outDir->GetDirectory("eu");
  auto ee_dir15 = outDir->GetDirectory("ee2015");
  auto uu_dir15 = outDir->GetDirectory("uu2015");
  auto eu_dir15 = outDir->GetDirectory("eu2015");
  auto ee_dir16 = outDir->GetDirectory("ee2016");
  auto uu_dir16 = outDir->GetDirectory("uu2016");
  auto eu_dir16 = outDir->GetDirectory("eu2016");

  for ( auto const& entry : container ) {
    if ( boost::algorithm::starts_with(entry.first,"ee") ) {
      if ( boost::algorithm::contains(entry.first,"2015") ) {
        ee_dir15->cd();
      }
      else if ( boost::algorithm::contains(entry.first,"2016") ) {
        ee_dir16->cd();
      }
      else {
        ee_dir->cd();
      }
      entry.second->Write();
    }
    else if ( boost::algorithm::starts_with(entry.first,"uu") ) {
      if ( boost::algorithm::contains(entry.first,"2015") ) {
        uu_dir15->cd();
      }
      else if ( boost::algorithm::contains(entry.first,"2016") ) {
        uu_dir16->cd();
      }
      else {
        uu_dir->cd();
      }
      entry.second->Write();
    }
    else if ( boost::algorithm::starts_with(entry.first,"eu") ) {
      if ( boost::algorithm::contains(entry.first,"2015") ) {
        eu_dir15->cd();
      }
      else if ( boost::algorithm::contains(entry.first,"2016") ) {
        eu_dir16->cd();
      }
      else {
        eu_dir->cd();
      }
      entry.second->Write();
    }
    else {
      TL::Fatal(__PRETTY_FUNCTION__,"Bad channel for",entry.first);
    }
  }
}

#endif
